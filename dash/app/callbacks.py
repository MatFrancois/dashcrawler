import base64
import copy
import io
import pdb
import time
from datetime import date, datetime, timedelta

import dash
# from dash import dcc, html
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import numpy as np
import pandas as pd
import plotly.express as px
import sqlalchemy as sql
from dash.dependencies import ClientsideFunction, Input, Output, State

import index
import utils as f
from concurrence import Concurrence
from hub import Hub
from main_graph import Main_Graph
from server import app


@app.callback(  # gestion de la page
    Output('page-content', 'children'),
    [Input('url', 'pathname')]
)
def display_page(pathname):
    if pathname == '/hub':
        return Hub()
    elif pathname == "/concurrence":
        return Concurrence()
    else:
        return Main_Graph()


@app.callback(  # chargement des données
    [
        Output("table1_inv", 'children'),
        Output("toast_error", "is_open")
    ],
    [
        Input("main_submit", "n_clicks"),
        Input('upload-data', 'contents')
    ],
    [
        State('upload-data', 'filename')
    ]
)
def starter(val, list_of_contents, list_of_names):
    if list_of_contents is not None:
        try:
            dfs = [f.parse_contents(c, n) for c, n in zip(list_of_contents, list_of_names)]
            con = f.connect()
            for d in dfs:
                f.push_to_db(d, con)
            df = f.extracter_transformer(con)
            return [df.to_json(orient='split'), None]
        except Exception:
            print(f'error: {str(Exception)}')
            return [None, True]

    elif val is not None:
        print("click")
        con = f.connect()
        df = f.extracter_transformer(con)

        return [df.to_json(orient='split'), None]

    else:
        return [None, None]

# change table's month order
# @app.callback(
#     [
        
#     ],
#     [
#         Input("month_order", "n_clicks"),
#     ]
# )

@app.callback(  # informations
    Output("popover", "is_open"),
    [Input("popover-target", "n_clicks")],
    [State("popover", "is_open")],
)
def toggle_popover(n, is_open):
    return not is_open if n else is_open


@app.callback(  # chargement des selecteurs
    [
        Output('time_selector', 'options'),
        Output("year_ref", "options"),
        Output("y_drop", "options"),
        Output("y_drop", "value"),
        # Output("y_drop", "value"),

    ],
    [
        Input("table1_inv", "children")
    ]
)
def feed_check_list(don):
    if don is None:
        return None
    print("update")
    df = pd.read_json(don, orient='split')

    yr = df.year.unique().tolist()
    YEAR_STATUS = [{"label": str(y), "value": str(y)} for y in np.sort(yr)]
    col = df.columns
    col = col.drop(['time', "week", 'date', 'year', 'day',
                    'date_same_year', 'month', 'index', 'month_str'])
    child = [dict(label=n, value=n) for n in col]
    if "total" in col:
        value = "total"

    return [YEAR_STATUS, YEAR_STATUS, child, value]


@app.callback(  # modification du tableau
    [
        Output("table_zone", "children"),
        Output("table_tot", "children")
    ],
    [
        Input("table1_inv", "children"),
        Input('daq_month', "value"),
    ]
)
def feed_table(df, mv):
    if df is None:

        return None
    df, tot = f.create_table(df, mv)

    df = pd.DataFrame(df.to_dict('records'))

    # modifier le df pour qu'il n'ait que des string number sep
    for col in df:
        if col != 'year':
            df[col] = df[col].apply(lambda x: format(x, ',.0f'))
    
    print(df)
    table = dbc.Table.from_dataframe(
        df,
        dark=True,
        hover=True,
        responsive=True,
        striped=True,
        bordered=True
    )
    tot = pd.DataFrame(tot.to_dict('records'))
    
    # modifier le df pour qu'il n'ait que des string number sep
    for col in tot:
        if col != 'year':
            tot[col] = tot[col].apply(lambda x: format(x, ',.0f'))

    print(tot)
    table_tot = dbc.Table.from_dataframe(
        tot,
        hover=True,
        responsive=True,
        striped=True,
        bordered=True,
        style={'width': '100%'}
    )
    return [table, table_tot]


@app.callback(  # modification du graph
    [
        Output("count_graph", "figure")
    ],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")

    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_graph(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None):
        figure = f.init_feed_graph(
            selection, y_ref, ordo, slider, df, "date_same_year")
        return [figure]
    else:
        return None


@app.callback(  # modification du graph tab 2 mois
    [
        Output("count_graph_mois", "figure")
    ],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")

    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_graph_mois(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None):
        figure = f.init_feed_graph(
            selection, y_ref, ordo, slider, df, "month_str")
        return [figure]
    else:
        return None


@app.callback(  # modification du graph tab 3 semaine
    [
        Output("count_graph_sem", "figure")
    ],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")

    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_graph_sem(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None):

        figure = f.init_feed_graph(selection, y_ref, ordo, slider, df, "week")
        return [figure]
    else:
        return None


@app.callback(  # focus on
    [
        Output("slider_zone", "children")
    ],
    [
        Input("table1_inv", "children")
    ]
)
def create_slider(don):
    if don is not None:
        month = {
            1: "Ja",
            2: "Fe",
            3: "Ma",
            4: "Av",
            5: "Ma",
            6: "Ju",
            7: "Ju",
            8: "Au",
            9: "Se",
            10: "Oc",
            11: "No",
            12: "De",
        }

        return [
            html.Div(
                [
                    dbc.FormGroup(
                        [
                            html.Hr(),
                            html.H5('Focus Sur', className=("space")),
                            dcc.RangeSlider(
                                min=1,
                                max=12,
                                marks=month,
                                value=[1, 12],
                                step=None,
                                className="custom-range space",
                                id="slider_month"
                            )
                        ]
                    )
                ], className="col-12"
            )
        ]

    return None


@app.callback(  # evo zone
    Output("evo_zone", "children"),
    Input("table1_inv", "children")
)
def create_evo_zone(df):
    if df is None:
        return None
    return [
        html.H5("Détails", className="space"),
        html.Div(
            html.Div(
                dbc.Tabs(
                    [
                        dbc.Tab(dcc.Graph(id="evo_graph"),
                                label="Global", className="col-sm-12"),
                        dbc.Tab(dcc.Graph(id="evo_graph_mois"),
                                label="Mois"),
                        dbc.Tab(dcc.Graph(id="evo_graph_sem"),
                                label="Semaine")
                    ]), className="col-9 col-xs-12 col-sm-12 space"
            ),
            className="row"
        )
    ]


@app.callback(  # evo graph mois
    [Output("evo_graph_mois", "figure")],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")
    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_evo_graph_mois(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None) and (ordo is not None) and (df is not None):
        figure = f.init_feed_graph(selection, y_ref, ordo, slider, df, agg="month_str",
                                   data_type="scatter", mode="lines+markers", color="#325D88")
        return [figure]
    else:
        return None


@app.callback(  # evo graph semaine
    [Output("evo_graph_sem", "figure")],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")
    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_evo_graph_sem(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None) and (ordo is not None) and (df is not None):
        figure = f.init_feed_graph(selection, y_ref, ordo, slider, df, agg="week",
                                   data_type="scatter", mode="lines+markers", color="#325D88")
        return [figure]
    else:
        return None


@app.callback(  # evo graph
    [Output("evo_graph", "figure")],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")
    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_evo_graph(selection, y_ref, ordo, slider, df):
    if (selection is not None) and (y_ref is not None) and (ordo is not None) and (df is not None):
        figure = f.init_feed_graph(selection, y_ref, ordo, slider, df, agg="date_same_year",
                                   data_type="scatter", mode="lines+markers", color="#325D88")
        return [figure]
    else:
        return None


@app.callback(  # modification du pie
    [
        Output("pie_graph", "figure")
    ],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("slider_month", "value")
    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_pie(selection, y_ref, ordo, slider, df):
    if selection is None or y_ref is None or ordo is None or df is None:
        return None
    df = pd.read_json(df, orient='split')
    selection.append(y_ref)
    selection = list(map(lambda x: int(x), selection))
    
    g = df[df.year.isin(selection)]
    yr = g.year.unique().tolist()
    if int(y_ref) in yr:
        yr.remove(int(y_ref))

    color = px.colors.qualitative.Pastel1

    col = g.columns.drop(["total", "week", "index", 'time', 'date',
                          'year', 'day', 'month_str', 'date_same_year', 'month'])

    dfpie = g.groupby(['month', 'year']).sum().reset_index()

    # tri sur les mois
    dfpie = dfpie[dfpie.month.isin(slider)]

    dfpie = dfpie.groupby('year').sum().reset_index()
    dfpie.year = dfpie.year.astype(str)  # conversion en string
    data = [
        dict(
            type="pie",
            values=dfpie.loc[dfpie.year == y_ref][col].values[0],
            labels=col,
            hoverinfo="text+value+percent",
            hole=0.5,
            marker=dict(colors=color),
        )
    ]

    layout2 = dict(
        autosize=True,
        automargin=True,
        margin=dict(l=30, r=30, b=20, t=40),
        # hovermode="x unified",
        plot_bgcolor="#F9F9F9",
        paper_bgcolor="#F9F9F9",
        legend=dict(font=dict(size=10), orientation="h"),
        title=f"Répartition du chiffre {ordo}",

    )
    figure = dict(data=data, layout=layout2)

    # ajouter une alerte si jamais il n'y a pas de données sur la période demandée
    return [figure]


@app.callback(  # cumsum zone
    Output("cumsum_zone", "children"),
    Input("table1_inv", "children")
)
def create_cumsum_zone(df):
    if df is None:
        return None
    
    return [
        html.Div(
            html.Div(
                dbc.Tabs(
                    [
                        dbc.Tab(dcc.Graph(id="cumsum_graph"),
                                label="Global", className="col-sm-12"),
                        dbc.Tab(dcc.Graph(id="cumsum_graph_mois"),
                                label="Mois"),
                        dbc.Tab(dcc.Graph(id="cumsum_graph_sem"),
                                label="Semaine")
                    ]), className="col-9 col-xs-12 col-sm-12 space"
            ),
            className="row"
        )
    ]

@app.callback(  # cumsum graph
    [
        Output("cumsum_graph", "figure"),
        Output("cumsum_graph_mois", "figure"),
        Output("cumsum_graph_sem", "figure")
    ],
    [
        Input("time_selector", "value"),
        Input("year_ref", "value"),
        Input("y_drop", "value"),
        Input("daq_month", "value"),
        # Input("slider_month", "value")
    ],
    [
        State("table1_inv", "children")
    ]
)
def feed_cumsum_graph(selection, y_ref, ordo, mv, df):
    print(mv)
    if (selection is not None) and (y_ref is not None) and (ordo is not None) and (df is not None) and (mv is not None):
        print(mv)
        fig_glob = f.feed_graph_cumsum(selection, y_ref, ordo, df, mv=mv, agg="date_same_year",
                                       data_type="scatter", mode="lines+markers", color="#325D88")
        fig_mois = f.feed_graph_cumsum(selection, y_ref, ordo, df, mv=mv, agg="month_str",
                                       data_type="scatter", mode="lines+markers", color="#325D88")
        fig_sem = f.feed_graph_cumsum(selection, y_ref, ordo, df, mv=mv, agg="week",
                                      data_type="scatter", mode="lines+markers", color="#325D88")
        return [fig_glob, fig_mois, fig_sem]
    else:
        return None


###################################
###### concurrence callbacks ######
###################################


@app.callback(  # chargement des données
    Output("table2_inv", 'children'),
    [
        Input("scrap_bd_load", "n_clicks")
    ]
)
def load_scraper(val):
    if val is None:
        return None
    print("click")
    con = f.connect()
    df = f.extracter_scraping(con)
    print(df)
    return df.to_json(orient='split')

@app.callback(# feed dropdown
    [
        *[Output(f'{h}_drop',r) for h in ['ceitya', 'ibis', 'sterne', 'seaview'] for r in ['options','value']],
        *[Output(f'perso_{h}_drop',r) for h in ['ceitya', 'ibis', 'sterne'] for r in ['options','value']]
    ],

    Input("table2_inv", "children")
)
def feed_dropdown(df):
    if df is None:
        return None
    df = pd.read_json(df, orient='split')

    res = []
    hotels = ['ceitya', 'ibis', 'sterne', 'seaView']
    for site in ['booking', 'perso']:
        for hotel in hotels:
            if (site == 'perso') & (hotel == 'seaView'):
                continue
            rooms = f.get_room(df, hotel, site=site)

            options = [{'label': 'Global', 'value': 'global'}]
            options.extend({'label': room, 'value': room} for room in rooms)
            res.extend((options, 'global'))
    return res

@app.callback(  # feed hotel data
    [Output(f"{h}_d{i+1}", r) for r in ['children', 'style'] for h in ['ceitya', 'ibis', 'sterne', 'seaview'] for i in range(6)],
    [
        Input('table2_inv', 'children'),
        Input('daq_min', 'value'),
        Input('ceitya_drop', 'value'),
        Input('ibis_drop', 'value'),
        Input('sterne_drop', 'value'),
        Input('seaview_drop', 'value')
    ])
def feed_price_booking(df, min_value, room_c='global', room_i='global', room_s='global', room_sv='global'):
    if df is None:
        return None
    df = pd.read_json(df, orient='split')
    
    mv = min_value == ['min_value']
    d = f.get_hotel_price(df, 'ceitya', min_value=mv, room=room_c) 
    d2 = f.get_hotel_price(df, 'ibis', min_value=mv, room=room_i) 
    d3 = f.get_hotel_price(df, 'sterne', min_value=mv, room=room_s) 
    d4 = f.get_hotel_price(df, 'seaView', min_value=mv, room=room_sv) 

    d_full = d + d2 + d3 + d4

    res = [html.H6(f'{prix} €') for prix in d_full]
    style = []
    for j in [d, d2, d3, d4]:
        for i in range(len(d)):
            comp = [d[i], d2[i], d3[i], d4[i]]
            while 0 in comp:
                comp.remove(0)
            if comp and j[i] == max(comp):
                style.append({'background': '#db8c8c'})
            elif len(comp) > 0 and j[i] == min(comp):
                style.append({'background': '#98db8c'})
            else:
                style.append(None)
    res += style
    return res

@app.callback(  # feed hotel data
    [Output(f"perso_{h}_d{i+1}", r) for r in ['children', 'style'] for h in ['ceitya', 'ibis', 'sterne'] for i in range(6)],
    [
        Input('table2_inv', 'children'),
        Input('perso_daq_min', 'value'),
        Input('perso_ceitya_drop', 'value'),
        Input('perso_ibis_drop', 'value'),
        Input('perso_sterne_drop', 'value'),
    ])
def feed_price_perso(df, min_value, room_c='global', room_i='global', room_s='global'):
    if df is None:
        return None
    df = pd.read_json(df, orient='split')
    mv = min_value == ['min_value']
    d = f.get_hotel_price(df, 'ceitya', min_value=mv, room=room_c, site='perso') 
    d2 = f.get_hotel_price(df, 'ibis', min_value=mv, room=room_i, site='perso') 
    d3 = f.get_hotel_price(df, 'sterne', min_value=mv, room=room_s, site='perso') 

    d_full = d + d2 + d3

    res = [html.H6(f'{prix} €') for prix in d_full]
    style = []
    for j in [d, d2, d3]:
        for i in range(len(d)):
            comp = [d[i], d2[i], d3[i]]
            while 0 in comp:
                comp.remove(0)
            if comp and j[i] == max(comp):
                style.append({'background': '#db8c8c'})
            elif len(comp) > 0 and j[i] == min(comp):
                style.append({'background': '#98db8c'})
            else:
                style.append(None)
    res += style
    return res

@app.callback(  # feed date data
    [
        *[Output(f"d{i+1}", 'children') for i in range(6)],
        *[Output(f"perso_d{i+1}", 'children') for i in range(6)],
    ],
    [
        Input('table2_inv', 'children'),
    ]
)
def feed_date(df):
    return f.get_date() + f.get_date() if df is not None else None
