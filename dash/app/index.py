# from dash import dcc, html
import dash_html_components as html
import dash_core_components as dcc
import callbacks

from server import app

app.layout = html.Div(
   [
       html.Div(
           [
                html.Div(id = 'table1_inv'),
                html.Div(id = 'table2_inv'),
           ],
           style={'display': 'none'}
       ),
       dcc.Location(id = 'url', refresh = False),
       
       html.Div(id = 'page-content')

   ] 
)

