import dash, dash_bootstrap_components as dbc, dash_daq as daq
# from dash import dcc, html
import dash_html_components as html
import dash_core_components as dcc
from navbar import Navbar


nav = Navbar('Concurrence', '/concurrence')


load_data = html.Button(  # button
    id='scrap_bd_load',
    children='Charger la base',
    className="btn btn-primary btn-sm space"
)


def minmax(id_name='daq_min'):
    return html.Div(
        [
            dbc.Checklist(
                id=id_name,
                options=[{"label": "max / min", "value": "min_value"}],
                value=['min_value'],
                switch=True,
                className="switcher"
            )
        ], className='col-3 space'
    )

def create_price_cell(hotel_name, perso=False):
    """create cell of each hotel on data comparing 

    Args:
        hotel_name (str): hotel name
        perso (bool, optional): is it the perso part ? Defaults to False.

    Returns:
        html.Div: html Div output : html.Div(row name, price * 6, dropdown)
    """
    nom = f'perso_{hotel_name.lower()}' if perso else hotel_name.lower()
    price = [html.Div(html.H6(".. €"),className="mini_container col-1", id=f"{nom}_d{i+1}") for i in range(6)]
    
    return html.Div(
        [
            html.H5(hotel_name, className="col-2 medium_container"),
            *price,
            html.Div(dcc.Dropdown(id=f"{nom}_drop",className="drop"),className="col-3"),
        ], className='row space'
    )

# hr
# header + min max
# h5 + prix + drop down
def create_header(perso=False):
    prefix = 'perso_' if perso else ''
    return [html.Div(html.H6('..', id=f'{prefix}d{i+1}'), className='mini_container col-1 header') for i in range(6)]

perso_header = html.Div([
    html.H5(' ', className="col-2 mc"), # useless div
    *create_header(True),
    minmax('perso_daq_min')
],  className='row space')

perso_prices = [create_price_cell(hotel, True) for hotel in ['Ceitya', 'Ibis', 'Sterne']]

booking_header = html.Div([
    html.H5(' ', className="col-2 mc"), # useless div
    *create_header(),
    minmax('daq_min')
],  className='row space')

booking_prices = [create_price_cell(hotel) for hotel in ['Ceitya', 'Ibis', 'Sterne', 'seaView']]



body_comp = html.Div([
    html.Div([
        html.Div([html.Div(load_data, className='col-9 col-xs-12')], className='row space'),
        html.Hr(),
        
        # Booking zone
        html.H3('Booking'),
        booking_header,
        *booking_prices,
        
        html.Hr(),
        
        # Perso zone
        html.H3('Perso'),
        perso_header,
        *perso_prices
        
    ], className="container")
], className='container-fluid')


def Concurrence():
    return html.Div(
        children=[
            nav,
            body_comp
        ]
    )
