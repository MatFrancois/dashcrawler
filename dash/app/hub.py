import dash_html_components as html

from navbar import Navbar

nav = Navbar('Home', '/hub')

# main figures !

def Hub():
    return html.Div(
        children = [
            nav,
            html.Div(
                id = 'table_zone'
            )
        ]
    )
