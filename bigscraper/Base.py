import time, requests, json

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from fake_useragent import UserAgent
from bs4 import BeautifulSoup

class MediumScraper:
    """
    A class to get a selenium driver and request Booking rooms name & prices of desired hotels.
    ...
        
    Examples
    --------
    Get cleaned trs data
    
    >>> MyCrawler = MediumScraper()
    >>> url = 'https://www.booking.com/hotel/fr/la-sterne.fr.html?checkin=2022-5-30;checkout=2022-5-31;group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;'
    >>> MyCrawler.get(url)
    >>> sc = BeautifulSoup(MyCrawler.driver.page_source, 'html.parser')
    """
    def __init__(self):
        self.options = Options()
        self.ua = UserAgent()
        self.userAgent = self.ua.random
        print(self.userAgent)
        self.options.add_argument(f'user-agent={self.userAgent}')
        self.options.add_argument('--disable-blink-features=AutomationControlled')
        self.caps = DesiredCapabilities().CHROME
        self.caps["marionette"]=True

        self.driver = webdriver.Remote(
            "http://selenium:4444/wd/hub",
            desired_capabilities = self.caps,
            options = self.options
        )

    def get(self, url=''):
        """get url

        Args:
            url (str, optional): website url. Defaults to ''.
        """
        self.driver.get(url)

    def extract(self, xpath):
        """returns the text of the element with the given xpath

        Args:
            xpath (str): xpath of the element

        Returns:
            list: list of text of the elements regarding the xpath
        """
        return [t.text for t in self.driver.find_elements_by_xpath(xpath)]

    def bs(self):
        """returns a bs object"""
        return BeautifulSoup(self.driver.page_source, 'html.parser')

    def bsBookingExtract(self, bso=None):
        """extract from a bs object

        :param regex: regex
        :param name: class or id
        :param tag: tag to extract
        :param bso: bs object
        :return:
        """
        if bso is None:
            bso = self.bs()
            
        top_table = bso.find_all('table', {'class': 'hprt-table hprt-table-long-language'})
        
        if len(top_table)>0:
            return self.extractPricesAndRoom(top_table)

    def extractPricesAndRoom(self, top_table):
        """extract prices and room name from each table in booking

        Args:
            top_table (list): list countaining tr elements

        Returns:
            list: list of names and prices
        """
        firsttr = top_table[0].find('tbody')
        res = firsttr.find_all('tr')
        outNom = []
        outPrix = []
        nom = None
        for t in res:
            prix = t.find('span', {'class': "prco-valign-middle-helper"})
            if (t.find('span', {'class': "hprt-roomtype-icon-link"}) is not None) and (prix is not None):
                nom = t.find('span', {'class': "hprt-roomtype-icon-link"})
            if prix is not None:
                outNom.append(nom.text.strip())
                outPrix.append(prix.text.strip().replace('€\xa0',''))
        return outNom, outPrix


def jsSterneExtract(url):
    """Extract data from own hotel website with requests

    Args:
        url (str): url website

    Returns:
        list: 2 lists of room name and prices
    """
    r = requests.get(url)
    if r.status_code != 200:
        return
    
    j = json.loads(r.text.strip(')('))
    time.sleep(1)
    try:
        long = len(j['Reponse']['items'][0]['metier']['items'])
        res = j['Reponse']['items'][0]['metier']['items']
        prix = []
        nom = []
        for l in range(long):
            prix.append(res[l]['Prix'])
            nom.append(res[l]['libelle'].lower())
            return nom, prix
    except IndexError as e:
        print(e)
        return

