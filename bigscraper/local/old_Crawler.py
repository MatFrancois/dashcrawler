

from Base import MediumScraper, jsSterneExtract
import time, pandas as pd, random as r
from datetime import datetime, timedelta
from bs4 import BeautifulSoup

# scrap zone
# booking
n = 1
data = []
print('======WHILE LOOP======')
while n<8:
    print('======BOOKING======')
    MyCrawler = MediumScraper()
    print("running... n = ", n)
 
    dateNjour = datetime.now() + timedelta(days=n)
    dateIn = str(dateNjour.year) + '-' + str(dateNjour.month) + '-' + str(dateNjour.day)
    dateN1jour = dateNjour + timedelta(days=1)
    dateOut = str(dateN1jour.year) + '-' + str(dateN1jour.month) + '-' + str(dateN1jour.day)

    print('----RUNNING DATE: {}----'.format(dateIn))

    hotel = ['sterne','seaView','ibis','ceitya']
    urls = [
        'https://www.booking.com/hotel/fr/la-sterne.fr.html?checkin='+dateIn+';checkout='+dateOut+';group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',\
        'https://www.booking.com/hotel/fr/le-ker-louis.fr.html?checkin=' + dateIn + ';checkout=' + dateOut + ';group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',\
        'https://www.booking.com/hotel/fr/aa1106853803873.fr.html?checkin=' + dateIn + ';checkout=' + dateOut + ';group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',\
        'https://www.booking.com/hotel/fr/le-ceitya.fr.html?checkin=' + dateIn + ';checkout=' + dateOut + ';group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',
    ]

    for url, name in zip(urls, hotel):
        MyCrawler.get(url)
        try: 
            time.sleep(r.randint(3,5))
        except TypeError:
            time.sleep(4)
            print("type error")
        ret = MyCrawler.bsBookingExtract()
        print(ret)
        if ret is not None:
            nom, prix = ret
            if (len(prix) > 0):
                print('nb prix : ',name,' - ',len(nom))
                for nm, p in zip(nom, prix):
                    data.append({
                        'measurement': name,
                        'site': 'booking',
                        'bedType': nm,
                        'time': pd.Series(dateIn).astype('datetime64')[0],
                        'prix': int(p)
                    })
            else:
                data.append({
                    'measurement': name,
                    'site': 'booking',
                    'bedType': 'full',
                    'time': pd.Series(dateIn).astype('datetime64')[0],
                    'prix': 0
                })
        else:
            data.append({
                'measurement': name,
                'site': 'booking',
                'bedType': 'full',
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': 0
            })
    
    # print('----PRICE FOUND: {}----'.format(len(prix)))

    print('======IBIS PERSO======')
    # ibis perso part
    ibis = "https://all.accor.com/ssr/app/accor/rates/8951/index.fr.shtml?dateIn=" + dateIn + "&nights=1&compositions=1&stayplus=false&destination=Saint%20gilles%20croix%20de%20vie,%20France"
    MyCrawler.get(ibis)
    try:
        time.sleep(r.randint(8,10))#time.sleep(5)
    except TypeError:
        time.sleep(10)
        print("type error a")
    sc = BeautifulSoup(MyCrawler.driver.page_source, 'html.parser')
    chambres = sc.find_all(class_='room-info__title')
    if (chambres == []):
        data.append({
            'measurement': 'ibis',
            'site': 'perso',
            'bedType': 'full',
            'time': pd.Series(dateIn).astype('datetime64')[0],
            'prix': 0
        })
    else:
        room = sc.find_all(class_='room-info')
        try:
            prix = [r.find(class_='reference-price').find(class_='booking-price__number').text for r in room]
        except:
            prix = [r.find(class_='booking-price__number').text for r in room]
        for chambre, tarif in zip(chambres, prix):
            data.append({
                'measurement': 'ibis',
                'site': 'perso',
                'bedType': chambre.text,
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': int(tarif)
            })
    # print('----PRICE FOUND: {}----'.format(len(prix)))

    print('======CEITYA PERSO======')
    # ceitya perso part
    dateIn = str(dateNjour.day) + '-' + str(dateNjour.month) + '-' + str(dateNjour.year)
    dateOut = str(dateN1jour.day) + '-' + str(dateN1jour.month) + '-' + str(dateN1jour.year)
    url = 'https://secure.reservit.com/fo/booking/2/170968/availability?nbroom=1&specialMode=default&begindate='+ dateIn +'&tmonth=07&tyear=2020&hotelid=170968&numnight=1&m=booking&roomAge1=40,40&langcode=FR&enddate=' + dateOut
    MyCrawler.get(url)
    try:
        time.sleep(r.randint(5,7))
    except TypeError:
        time.sleep(6)
        print("type error b")
    sc = BeautifulSoup(MyCrawler.driver.page_source, 'html.parser')

    # room zone
    nroom = sc.find_all(id='booking-room-component')

    if len(nroom) > 0:
        for r in nroom:
            for i in r.find_all(class_='name'):
                chambres = i.find(class_="ng-star-inserted").text
            prix = r.find(class_="price-info-price ng-star-inserted").text
            prix = prix.replace('\xa0€','').split(',')[0]
            data.append({
                'measurement': 'ceitya',
                'site': 'perso',
                'bedType': chambres.lower(),
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': int(prix)
            })
    else:
        data.append({
            'measurement': 'ceitya',
            'site': 'perso',
            'bedType': 'full',
            'time': pd.Series(dateIn).astype('datetime64')[0],
            'prix': 0
        })
    # print('----PRICE FOUND: {}----'.format(len(nroom)))

    n +=1
    MyCrawler.driver.close()

print('======STERNE PERSO======')
## sterne perso
n = 0
while n>20:
    print(n)
    dateNjour = datetime.now() + timedelta(days=n)
    m = str(dateNjour.month)
    m = m if len(m) == 2 else '0' + m
    d = str(dateNjour.day)
    d = d if len(d) == 2 else '0' + d
    dateIn = str(dateNjour.year) + '-' + m + '-' + d
    dateN1jour = dateNjour + timedelta(days=1)
    dateOut = str(dateN1jour.year) + '-' + str(dateN1jour.month) + '-' + str(dateN1jour.day)

    url = 'https://etape-rest.for-system.com/index.aspx?ref=json-catalogue-etape22&q=fr,HRIT-30431,vendee,' + dateIn + ',1,1,,*,*,0'
    ret = jsSterneExtract(url)
    if ret is not None:
        nom, prix = ret
        for nm, p in zip(nom, prix):
            data.append({
                'measurement': 'sterne',
                'site': 'perso',
                'bedType': nm,
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': int(p)
            })
    else:
        data.append({
            'measurement': 'sterne',
            'site': 'perso',
            'bedType': 'full',
            'time': pd.Series(dateIn).astype('datetime64')[0],
            'prix': 0
        })
    n += 1
    # print('----PRICE FOUND: {}----'.format(len(prix)))

df = pd.DataFrame(data)


print('======PUSH NEW DATA======')
print(df)