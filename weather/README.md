# API METEO

Package permettant la récupération des données météos de Saint Gilles Croix de Vie depuis l'api de Windy.

Historisation : Quotidienne

Localisation : db_pi.weather

## Format

Historisation de chaque jours toutes les 3h, 8 individus par jour.
La catégorie **icon** correspond à un indice de 1 à 20 sur la qualité du temps.

```
 index |           date            |        temp        | icon | wind_kts | rain_mm |          dat_exec
-------+---------------------------+--------------------+------+----------+---------+----------------------------
       | 2021-04-24T05:00:00+02:00 | 13.350000000000026 |    2 |      6.9 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T08:00:00+02:00 |              12.75 |    3 |      5.7 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T11:00:00+02:00 |              15.75 |    2 |      5.9 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T14:00:00+02:00 | 17.350000000000026 |    3 |      4.7 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T17:00:00+02:00 |              16.25 |    2 |      2.8 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T20:00:00+02:00 | 16.550000000000008 |    1 |      3.3 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-24T23:00:00+02:00 | 15.950000000000045 |    1 |        5 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-25T02:00:00+02:00 |              15.75 |    2 |      7.7 |       0 | 2021-04-24 12:33:38.969057
       | 2021-04-25T05:00:00+02:00 | 13.550000000000011 |    1 |      7.3 |       0 | 2021-04-25 11:06:53.695179
       | 2021-04-25T08:00:00+02:00 |              11.75 |    1 |      6.7 |       0 | 2021-04-25 11:06:53.695179
```

_cron quotidien_
