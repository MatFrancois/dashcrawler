#!/bin/bash
echo "--------START PROCESSING AT: $(date "+%Y-%m-%d %H:%M:%S")--------"
echo "building dash image"
docker build ~/Documents/hotel_monitoring/dash/. --tag dash_img

echo "building scraper image"
docker build ~/Documents/hotel_monitoring/bigscraper/. --tag maatzum/bigscraper-pi

echo "building scraper image"
docker build ~/Documents/hotel_monitoring/weather/. --tag weather_img

echo "running docker compose"
cd ~/Documents/hotel_monitoring/
docker-compose up -d

echo "---------END PROCESSING AT: $(date "+%Y-%m-%d %H:%M:%S")---------"
